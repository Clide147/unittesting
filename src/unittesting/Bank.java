package unittesting;

import java.util.ArrayList;

public class Bank
{

//    private String firstname;
//    private String lastname;
    private double balance = 0.0;
//    private double initialBalance;

    private ArrayList Accounts;

    public Bank()
    {
        Accounts = new ArrayList();
    }

    public void addAccount(Account newAccount)
    {
        Accounts.add(newAccount);
    }

    public int getNumberOfAccounts()
    {
        return this.Accounts.size();
    }

    public void addAccount(String firstName, String lastName)
    {
//        this.firstname = firstName;
//        this.lastname = lastName;

        Account temp = new Account(firstName, lastName, balance);
        Accounts.add(temp);
    }

    public void addAccount(String firstName, String lastName, double initialBalance)
    {
//        this.firstname = firstName;
//        this.lastname = lastName;
//        this.balance = initialBalance;

        Account temp = new Account(firstName, lastName, initialBalance);
        Accounts.add(temp);
    }
}


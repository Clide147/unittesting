package unittesting;

public class Account
{
    private String fName;
    private String lName;
//    private  double initialBalance;
    private double balance;
    private int id;

    public Account(double i)
    {
        this.balance = i;
        this.id = 1;
    }

    public Account()
    {
        this.balance = 0;
    }

    // Not sure of this.
    public Account(String firstName, String lastName)
    {
        setFirstName(firstName);
        setLastName(lastName);
    }

    public Account(String firstName, String lastName, double v)
    {
        setFirstName(firstName);
        setLastName(lastName);
        SetBalance(v);
    }

    public double getBalance()
    {
        return this.balance;
    }

    public void SetBalance(double i)
    {
        this.balance = i;
    }

    public void setFirstName(String firstName)
    {
         fName = firstName;
    }

    public void setLastName(String lastName)
    {
         lName = lastName;
    }

    public void deposit(double i)
    {
        balance += i;
    }

    public String getFirstName()
    {
        return this.fName;
    }

    public String getLastName()
    {
        return this.lName;
    }

    public int getId()
    {
        return this.id;
    }
}

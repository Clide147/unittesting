package unittesting;
import junit.framework.TestCase;

public class Tests extends TestCase
{
    // This is your given one!
    public void testMakeAccount()
    {
        Account mitchell = new Account();
        assertEquals(mitchell.getClass(), Account.class);
    }
    public void testMakeAccount2()
    {
        Account mitchell = new Account(1000.0);
        assertEquals("This should return 1000 because I initialized it with 1000", 1000.0, mitchell.getBalance());
    }
    public void testGetBalance()
    {
        Account mitchell = new Account();
        assertEquals("This should return 0 because I just initialized it.",0.0, mitchell.getBalance());
    }
    public void testSetBalance()
    {
        Account mitchell = new Account();
        mitchell.SetBalance(10);
        assertEquals("This should return 0 because I just initialized it.",10.0, mitchell.getBalance());
    }
    public void testAccountSettings(){
        Account mitchell = new Account();
        mitchell.setFirstName("Mitchell");
        mitchell.setLastName("Bosman");
        mitchell.deposit(1000.0);
        assertEquals("Should return my first name","Mitchell",mitchell.getFirstName());
        assertEquals("Should return my last name","Bosman",mitchell.getLastName());
        assertEquals("Should return my total balance",1000.0,mitchell.getBalance());
    }
    public void testDefaultAccountId(){
        Account mitchell = new Account();
        assertEquals("It should return a value", 0,mitchell.getId());
    }
    public void testMakeBank(){
        Bank mitchBank = new Bank();
        assertEquals("Should make a Bank Class",mitchBank.getClass(), Bank.class);
    }
    public void testAddAccountToBank(){
        Bank mitchBank = new Bank();
        Account newAccount = new Account();
        mitchBank.addAccount(newAccount);
        assertEquals("There should be one account in the bank.",1,mitchBank.getNumberOfAccounts());
    }

    // New Test Case:
    public void testAddAccountToBank2(){
        Bank bank = new Bank();
        Account one = new Account("Mitchell","Bosman");// should be (firstname, lastname)
        Account two = new Account("Mitchell","Bosman",1231.23); // should be (firstname, lastname, initialbalance)
        bank.addAccount(one);
        bank.addAccount(two);
        assertEquals("This should return 2", 2, bank.getNumberOfAccounts());

        bank.addAccount("Mitchell", "Bosman"); // should be (firstname, lastname)
        bank.addAccount("Mitchell", "Bosman", 1000.00); // should be (firstname, lastname, initialbalance)
        assertEquals("This should return 4", 4, bank.getNumberOfAccounts());
    }

    public void testAccountIds(){
        Bank bank = new Bank();
        Account one = new Account("Mitchell","Bosman");// should be (firstname, lastname)
        Account two = new Account("Mitchell","Bosman",1231.23); // should be (firstname, lastname, initialbalance)
        bank.addAccount(one);
        bank.addAccount(two);
        assertEquals("This should return 1",1,one.getId());
        assertEquals("This should return 2",2,two.getId());

    }
}